namespace :kadai4 do
    desc '集計'
    task :aggregate => :environment  do
        # テーブルのコピー
        #ActiveRecord::Base.connection.execute('DELETE FROM yester_products;')
        #ActiveRecord::Base.connection.execute('INSERT INTO yester_products SELECT * FROM products;')
        
        # 追加されたぶん
        added_products = Product.find_by_sql('SELECT * FROM products tp WHERE NOT EXISTS (SELECT * FROM yester_products yp WHERE tp.id=yp.id);');
        # 削除されたぶん
        deleted_products = Product.find_by_sql('SELECT * FROM yester_products yp WHERE NOT EXISTS (SELECT * FROM products tp WHERE tp.id=yp.id);');

        added_products.each do |product|
            change = Change.new(
                date:Time.current,
                action: 'add',
                product_id: product.id,
                name: product.name,
                description: product.description,
                price: product.price,
                image: product.image
            )
            change.save
        end

        deleted_products.each do |product|
            change = Change.new(
                date:Time.current,
                action: 'deleted',
                product_id: product.id,
                name: product.name,
                description: product.description,
                price: product.price,
                image: product.image
            )
            change.save
        end
        #Change.save


        #printf "chari da yoooooooooon"
    end
end
